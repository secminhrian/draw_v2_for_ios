enum BrushStyle: Int {
	case pen = 0
	case eraser = 1
}
