enum UserDefaultKeys: String {
	case colorKey = "Color"
	case penWidthKey = "PenWidth"
	case eraserWidthKey = "EraserWidth"
	case lastPaintModeIndexKey = "LastPaintModeIndex" //0 for pen, 1 for eraser
	case imageDataKey = "imageData"
}
