import UIKit
class CleanCanvasAction: ToolsActionProtocol {
	
	static var actionName = "Clean Canvas"
	var style = UIAlertActionStyle.default
	
	func run(mainCanvas: Canvas, callerController: ViewController) {
		mainCanvas.cleanCanvas()
	}
}
