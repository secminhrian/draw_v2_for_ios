import UIKit

class LineWidthPreviewer: UIImageView {
	
	fileprivate var firstPoint: CGPoint {
		get {
			return CGPoint(x: offset, y: offset)
		}
	}
	fileprivate var secondPoint: CGPoint {
		get {
			return CGPoint(x: bounds.width - offset, y: bounds.height - offset)
		}
	}
	fileprivate var offset: CGFloat = 0
	fileprivate var size: Int = 10 {
		didSet {
			offset = CGFloat(size/2)
		}
	}
	
	func preview(size: Int, using mode: BrushStyle, withColor color: UIColor) {
		self.size = size
		let context = beginAndGetContext()
		setup(context: context, using: mode, withColor: color)
		drawLine(context)
	}
	
	fileprivate func beginAndGetContext() -> CGContext {
		UIGraphicsBeginImageContext(CGSize(width: self.bounds.width, height: self.bounds.height))
		return UIGraphicsGetCurrentContext()!
	}
	
	fileprivate func setup(context: CGContext,
	                       using mode: BrushStyle,
	                       withColor color: UIColor) {
		self.backgroundColor = (mode == .pen ? UIColor.white : color)
		context.setStrokeColor(mode == .pen ? color.cgColor : UIColor.white.cgColor)
		context.setLineCap(.round)
		context.setLineJoin(.round)
		context.setShouldAntialias(true)
	}
	
	fileprivate func drawLine(_ context: CGContext) {
		context.setLineWidth(CGFloat(size))
		context.addLines(between: [firstPoint, secondPoint])
		context.strokePath()
		image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
	}
}
