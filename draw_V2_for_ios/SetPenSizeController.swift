import UIKit

class SetPenSizeController: UIViewController {
	
	var defaultSize = 10
	var previewMode = BrushStyle.pen
	var delegate: SendDataDelegate?
	var colorToUse: UIColor = .black
	
	func setup(defaultSize size: Int = 10, previewMode mode: BrushStyle = .pen, delegate: SendDataDelegate, colorToUse color: UIColor = .black) {
		self.defaultSize = size
		self.previewMode = mode
		self.delegate = delegate
		self.colorToUse = color
	}
	
	@IBOutlet weak var paintSizeSlider: UISlider! {
		didSet {
			paintSizeSlider.value = Float(defaultSize)
		}
	}
	@IBOutlet weak var previewer: LineWidthPreviewer! {
		didSet {
			self.previewer.preview(size: Int(paintSizeSlider.value), using: previewMode, withColor: colorToUse)
		}
	}
	
	@IBAction func navigaionButtonClicked(_ sender: UIBarButtonItem) {
		if sender.style == .done {
			delegate?.getFromPenSize(Int(paintSizeSlider.value))
		}
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func paintSizeSliderChanged(_ sender: UISlider) {
		previewer.preview(size: Int(sender.value), using: previewMode, withColor: colorToUse)
	}
}
