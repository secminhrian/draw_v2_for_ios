import UIKit

class ViewController: UIViewController, SendDataDelegate {
	
	//TODO: check position of segment
	
	// MARK:- drawing needed data
	fileprivate var lastPoint = CGPoint(x:0, y:0)
	fileprivate var currentPoint = CGPoint(x: 0, y: 0)
	
	//Mark:- Brushes
	let pen = Pen()
	let eraser = Eraser()
	
	
	
	// MARK:- mainCanvas
	@IBOutlet weak var mainCanvas: Canvas! {
		didSet {
			setupDataFromUserDefault()
		}
	}
	
	@IBOutlet weak var toolsBarItem: UIBarButtonItem!
	
	//MARK:- functions about PenSegment
	@IBOutlet weak var paintingSegment: UISegmentedControl!

	@IBAction func paintintModeChanged(_ sender: UISegmentedControl) {
		setPenSizeButton.title = "Set \(sender.titleForSegment(at: sender.selectedSegmentIndex)!) Size"
	}
	
	fileprivate var paintingMode: BrushStyle {
		get {
			return BrushStyle(rawValue: paintingSegment.selectedSegmentIndex)!
		}
	}
	
	fileprivate var currentlyUsingBrush: BaseBrush {
		get {
			return paintingMode == .pen ? pen : eraser
		}
	}
	
	//MARK:- functions about setPenSizeButton
	@IBOutlet weak var setPenSizeButton: UIBarButtonItem!
	
	private let actions:[String:ToolsActionProtocol] = [CleanCanvasAction.actionName:CleanCanvasAction(), SaveAction.actionName:SaveAction(), DeleteAction.actionName:DeleteAction(), ShareAciton.actionName:ShareAciton(), CancelAction.actionName:CancelAction()]
	
	//MARK:- when user click on tools barItem
	@IBAction func toolsItemClick(_ sender: UIBarButtonItem) {
		let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		actions.forEach { (actionLabel, action) in
			let alertAction = UIAlertAction(title: actionLabel, style: action.style) { action in
				self.actions[action.title!]!.run(mainCanvas: self.mainCanvas, callerController: self)
			}
			alertController.addAction(alertAction)
		}
		if UI_USER_INTERFACE_IDIOM() == .pad {
			alertController.popoverPresentationController?.barButtonItem = sender
		}
		present(alertController, animated: true, completion: nil)
	}
}

//MARK:- UserDefault save/read
extension ViewController {
	func setupDataFromUserDefault() {
		let userDefault = UserDefaults.standard
		pen.colorData = userDefault.array(forKey: UserDefaultKeys.colorKey.rawValue) as? [Float] ?? [0.f,0.f,0.f]
		pen.lineWidth = userDefault.integer(forKey: UserDefaultKeys.penWidthKey.rawValue)
		eraser.lineWidth = userDefault.integer(forKey: UserDefaultKeys.eraserWidthKey.rawValue)
		paintingSegment.selectedSegmentIndex = userDefault.integer(forKey: UserDefaultKeys.lastPaintModeIndexKey.rawValue)
		let imageData = userDefault.data(forKey: UserDefaultKeys.imageDataKey.rawValue)
		mainCanvas.setImage(with: imageData)
	}
	
	func saveDataToUserDefault() {
		let userDefault = UserDefaults.standard
		userDefault.set(pen.colorData, forKey: UserDefaultKeys.colorKey.rawValue)
		userDefault.set(pen.lineWidth, forKey: UserDefaultKeys.penWidthKey.rawValue)
		userDefault.set(eraser.lineWidth, forKey: UserDefaultKeys.eraserWidthKey.rawValue)
		userDefault.set(mainCanvas.imageData, forKey: UserDefaultKeys.imageDataKey.rawValue)
		userDefault.set(paintingSegment.selectedSegmentIndex, forKey: UserDefaultKeys.lastPaintModeIndexKey.rawValue)
	}
}

//MARK:- detecting drawing movement
extension ViewController {
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		lastPoint = touches.first!.location(in: mainCanvas)
		currentPoint = lastPoint
		currentlyUsingBrush.draw(from: lastPoint, to: currentPoint, on: mainCanvas)
	}
	
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		currentPoint = touches.first!.location(in: mainCanvas)
		currentlyUsingBrush.draw(from: lastPoint, to: currentPoint, on: mainCanvas)
		lastPoint = currentPoint
	}
}

//MARK:- get data from other screens
extension ViewController {
	func getFromPenSize(_ data: Int) {
		var brush = currentlyUsingBrush
		brush.lineWidth = data
	}
	
	func getFromPalette(_ data: [Float]) {
		pen.colorData = data
		paintingSegment.selectedSegmentIndex = 0
		paintintModeChanged(paintingSegment)
	}
}

//MARK:- when user clicks on palette button or set size button
extension ViewController {
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let targetController = (segue.destination as! UINavigationController).topViewController!
		if let stringIndentifier = segue.identifier {
			guard let indentifier = Segues(rawValue: stringIndentifier) else {
				return
			}
			switch indentifier {
			case .toPanSize:
				let controller = targetController as! SetPenSizeController
				controller.setup(defaultSize: currentlyUsingBrush.lineWidth, previewMode: paintingMode, delegate: self, colorToUse: pen.color)
			case .toPalette:
				let controller = targetController as! PaletteController
				controller.delegate = self
				controller.setStartColor(pen.colorData)
			}
		}
	}
}
//MARK:- Int extension to convert to Float more easily
extension Int {
	var f: Float {
		get {
			return Float(self)
		}
	}
}
