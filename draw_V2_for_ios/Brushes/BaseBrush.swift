import UIKit

protocol BaseBrush {
	var lineWidth: Int {set get}
	var colorData: [Float] {set get}
	var color: UIColor { get }
	var style: BrushStyle { get }
	func draw(from firstPoint: CGPoint, to secondPoint: CGPoint, on canvas: Canvas)
}
