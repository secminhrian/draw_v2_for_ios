import UIKit

class PaletteController: UIViewController, CustomViewControllerProtocol {
	
	var delegate: SendDataDelegate?
	
	@IBOutlet weak var RSlider: UISlider! {
		didSet {
			RSlider.value = startR
		}
	}
	@IBOutlet weak var GSlider: UISlider! {
		didSet {
			GSlider.value = startG
		}
	}
	@IBOutlet weak var BSlider: UISlider! {
		didSet {
			BSlider.value = startB
		}
	}
	@IBOutlet weak var colorPreviewer: UIImageView! {
		didSet {
			colorPreviewer.backgroundColor = UIColor(red: CGFloat(RSlider.value/255), green: CGFloat(GSlider.value/255), blue: CGFloat(BSlider.value/255), alpha: 1)
		}
	}
	
	private var startR: Float = 0
	private var startG: Float = 0
	private var startB: Float = 0
	
	@IBAction func sliderChanged() {
		colorPreviewer.backgroundColor = UIColor(red: CGFloat(RSlider.value/255), green: CGFloat(GSlider.value/255), blue: CGFloat(BSlider.value/255), alpha: 1)
	}
	
	@IBAction func barButtonClicked(_ sender: UIBarButtonItem) {
		if sender.style == .done {
			delegate?.getFromPalette([RSlider.value/255,GSlider.value/255, BSlider.value/255])
		}
		dismiss(animated: true, completion: nil)
	}
	
	func setStartColor(_ color: [Float]) {
		startR = color[0]*255
		startG = color[1]*255
		startB = color[2]*255
	}
}
