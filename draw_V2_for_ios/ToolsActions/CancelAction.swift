import UIKit

class CancelAction: ToolsActionProtocol {
	
	static var actionName = "Cancel"
	var style = UIAlertActionStyle.cancel
	
	func run(mainCanvas: Canvas, callerController: ViewController) {
		//intentional left empty
	}
}
