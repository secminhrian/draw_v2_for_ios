import UIKit
class DeleteAction: ToolsActionProtocol {
	
	static var actionName = "Delete"
	var style = UIAlertActionStyle.destructive
	
	private let alertTitle = "Are you sure to DELETE?"
	private let alertMessage = "This action is NOT recoverable."
	
	private let OKTitle = "OK"
	private let cancelTitle = "Cancel"
	
	func run(mainCanvas: Canvas, callerController: ViewController) {
		let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: OKTitle, style: .destructive) { action in
			mainCanvas.deleteImage()
		})
		alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: nil))
		callerController.present(alert, animated: true, completion: nil)
	}
}
