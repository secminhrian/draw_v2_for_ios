import UIKit
protocol ToolsActionProtocol {
	static var actionName: String { get }
	var style: UIAlertActionStyle { get }
	func run(mainCanvas: Canvas, callerController: ViewController) -> Void
}
