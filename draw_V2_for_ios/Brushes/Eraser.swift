import UIKit

class Eraser: BaseBrush {
	
	var style: BrushStyle {
		get {
			return .eraser
		}
	}
	var lineWidth: Int {
		didSet {
			if lineWidth == 0 {
				self.lineWidth = 10
			}
		}
	}
	var color: UIColor {
		get {
			return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
		}
	}
	var colorData: [Float] {
		set(newData) {
			if newData != [255.f, 255.f, 255.f] {
				self.colorData = [255.f, 255.f, 255.f]
			}
		}
		get {
			return self.colorData
		}
	}
	
	init(lineWidth: Int = 10, color: [Float] = [255.f, 255.f, 255.f]) {
		self.lineWidth = lineWidth
		self.colorData = color
	}
	
	func draw(from firstPoint: CGPoint, to secondPoint: CGPoint, on canvas: Canvas) {
		canvas.setUpThenDrawLine(from: firstPoint, to: secondPoint, withBrush: self)
	}
}
