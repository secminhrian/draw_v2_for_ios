import UIKit

class Canvas: UIImageView {
	
	var imageData: Data {
		get {
			return NSKeyedArchiver.archivedData(withRootObject: self.image as Any)
		}
	}
	
	fileprivate func setup(with brush: BaseBrush) throws {
		guard let context = UIGraphicsGetCurrentContext() else {
			throw Errors.unwrapNil
		}
		context.setLineCap(.round)
		context.setLineJoin(.round)
		context.setStrokeColor(brush.color.cgColor)
		context.setLineWidth(CGFloat(brush.lineWidth))
		context.setShouldAntialias(true)
	}

	func setUpThenDrawLine(from firstPoint: CGPoint, to secondPoint: CGPoint, withBrush brush: BaseBrush) {
		UIGraphicsBeginImageContext(CGSize(width: self.bounds.width, height: self.bounds.height))
		do {
			try setup(with: brush)
			try drawOldImageThenDrawLine(from: firstPoint, to: secondPoint)
		} catch Errors.unwrapNil {
			UIGraphicsBeginImageContext(CGSize(width: self.bounds.width, height: self.bounds.height))
			setUpThenDrawLine(from: firstPoint, to: secondPoint, withBrush: brush)
		} catch {
			print("unknown error")
		}
		UIGraphicsEndImageContext()
	}
	
	fileprivate func drawOldImageThenDrawLine(from firstPoint: CGPoint, to secondPoint: CGPoint) throws {
		guard let context = UIGraphicsGetCurrentContext() else {
			throw Errors.unwrapNil
		}
		self.image?.draw(in: self.bounds)
		context.addLines(between: [firstPoint, secondPoint])
		context.strokePath()
		self.image = UIGraphicsGetImageFromCurrentImageContext()
	}
	
	func setImage(with data:Data?) {
		if data != nil {
			self.image = NSKeyedUnarchiver.unarchiveObject(with: data!) as? UIImage
		} else {
			self.image = UIImage()
		}
	}
	
	func cleanCanvas() {
		self.image = UIImage()
		UIGraphicsEndImageContext()
	}
	
	func saveImage() {
		let userDefault = UserDefaults.standard
		userDefault.set(self.imageData, forKey: UserDefaultKeys.imageDataKey.rawValue)
		setImage(with: self.imageData)
	}
	
	func deleteImage() {
		cleanCanvas()
		UserDefaults.standard.removeObject(forKey: UserDefaultKeys.imageDataKey.rawValue)
	}
}
