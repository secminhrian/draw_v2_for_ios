import UIKit

protocol SendDataDelegate {
	func getFromPenSize(_ data: Int) -> Void
	func getFromPalette(_ data: [Float]) -> Void
}
