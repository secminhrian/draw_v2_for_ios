import UIKit

class Pen: BaseBrush {
	var lineWidth: Int {
		didSet {
			if lineWidth == 0 {
				self.lineWidth = 10
			}
		}
	}
	var colorData = [0.f, 0.f, 0.f]
	var color: UIColor {
		get {
			return UIColor(red: colorData[0].cgf, green: colorData[1].cgf, blue: colorData[2].cgf, alpha: 1)
		}
	}
	var style: BrushStyle {
		get {
			return .pen
		}
	}
	
	init(lineWidth: Int = 10, color: [Float] = [0.f, 0.f, 0.f]) {
		self.lineWidth = lineWidth
		self.colorData = color
	}
	
	func draw(from firstPoint: CGPoint, to secondPoint: CGPoint, on canvas: Canvas) {
		canvas.setUpThenDrawLine(from: firstPoint, to: secondPoint, withBrush: self)
	}
}
extension Float {
	var cgf: CGFloat {
		get {
			return CGFloat(self)
		}
	}
}
