import UIKit
class SaveAction: ToolsActionProtocol {
	
	static var actionName = "Save"
	var style = UIAlertActionStyle.default
	
	func run(mainCanvas: Canvas, callerController: ViewController) {
		mainCanvas.saveImage()
	}
}
