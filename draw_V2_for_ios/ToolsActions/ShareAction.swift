import UIKit
class ShareAciton: ToolsActionProtocol {
	
	static var actionName = "Share"
	var style = UIAlertActionStyle.default
	
	func run(mainCanvas: Canvas, callerController: ViewController) {
		var imageToSave: UIImage!
		if let originImage = mainCanvas.image {
			imageToSave = originImage
		} else {
			imageToSave = UIImage()
		}
		let shareController = UIActivityViewController(activityItems: [imageToSave], applicationActivities: nil)
		if UI_USER_INTERFACE_IDIOM() == .pad {
			shareController.popoverPresentationController?.barButtonItem = callerController.toolsBarItem
		}
		callerController.present(shareController, animated: true, completion: nil)
	}
}
